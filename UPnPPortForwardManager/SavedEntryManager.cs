﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UPnPPortForwardManager
{
    public static class SavedEntryManager
    {
        private static string _SettingsFile = string.Empty;
        private static HashSet<SavedEntry> _SavedEntries = new HashSet<SavedEntry>();
        private static string _FileName = "UPnPPortForwardManager.dat";
        private static string _PersonalDataPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        public static string SettingsFile
        {
            get
            {
                if (string.IsNullOrEmpty(_SettingsFile))
                {
                    FindExistingSavedEntries();
                }
                return _SettingsFile;
            }
        }

        public static HashSet<SavedEntry> SavedEntries
        {
            get
            {
                using (Stream stream = File.Open(_SettingsFile, FileMode.Open, FileAccess.Read))
                {
                    var binfmt = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    _SavedEntries = (HashSet<SavedEntry>)binfmt.Deserialize(stream);
                }

                return _SavedEntries;
            }
        }

        public static void Add(string desc, string extport, string intport, string proto)
        {
            Add(desc, int.Parse(extport), int.Parse(intport), proto);
        }

        public static void Add(string desc, int extport, int intport, string proto)
        {
            Add(new SavedEntry() { Description = desc, ExternalPort = extport, InternalPort = intport, Protocol = proto });
        }

        public static void Add(SavedEntry entry)
        {
            _SavedEntries.Add(entry);
            FlushFileToDisk();
        }

        public static void Remove(string desc, string extport, string intport, string proto)
        {
            Remove(desc, int.Parse(extport), int.Parse(intport), proto);
        }

        public static void Remove(string desc, int extport, int intport, string proto)
        {
            Remove(new SavedEntry(){ Description = desc, ExternalPort = extport, InternalPort = intport, Protocol = proto });
        }
        public static void Remove(SavedEntry entry)
        {
            _SavedEntries.Remove(entry);
            FlushFileToDisk();
        }

        private static void FlushFileToDisk()
        {
            using (Stream stream = File.Open(SettingsFile, FileMode.Open, FileAccess.Write))
            {
                var binfmt = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binfmt.Serialize(stream, _SavedEntries);
            }
        }

        private static void FindExistingSavedEntries()
        {
            if (File.Exists(Path.Combine(_PersonalDataPath, _FileName)))
            {
                _SettingsFile = Path.Combine(_PersonalDataPath, _FileName);
            }
            if (File.Exists(_FileName))
            {
                _SettingsFile = Path.GetFullPath(_FileName);
            }

            if (string.IsNullOrEmpty(_SettingsFile))
            {
                try
                {
                    File.Create(_FileName).Close();
                    _SettingsFile = Path.GetFullPath(_FileName);
                    FlushFileToDisk();
                }
                catch (UnauthorizedAccessException)
                {
                    File.Create(Path.Combine(_PersonalDataPath, _FileName)).Close();
                    _SettingsFile = Path.GetFullPath(Path.Combine(_PersonalDataPath, _FileName));
                    FlushFileToDisk();
                }
            }
        }

        [Serializable]
        public struct SavedEntry
        {
            public string Description;
            public int ExternalPort;
            public int InternalPort;
            public string Protocol;
        }
    }
}

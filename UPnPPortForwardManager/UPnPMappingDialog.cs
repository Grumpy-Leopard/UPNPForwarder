﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UPnPPortForwardManager
{
    public partial class UPnPMappingDialog : Form
    {
        public UPnPMappingDialog()
        {
            InitializeComponent();
            PopulateLocalIPv4Addresses();
        }

        private void UPnPMappingDialog_Load(object sender, EventArgs e)
        {
            this.Validating += new CancelEventHandler(UPnPMappingDialog_Validating);
            this.txtDescription.Validating += new CancelEventHandler(UPnPMappingDialog_Validating);
            this.txtExternalPort.Validating+= new CancelEventHandler(UPnPMappingDialog_Validating);
            this.cmbInternalClient.Validating+= new CancelEventHandler(UPnPMappingDialog_Validating);
            this.txtInternalPort.Validating += new CancelEventHandler(UPnPMappingDialog_Validating);
        }

        private bool ErrorsClean = true;
        void UPnPMappingDialog_Validating(object sender, CancelEventArgs e)
        {
            ErrorsClean = true;
            errorProvider1.Clear();
            ProcessEmptyTextValidation(txtDescription);
            ProcessComboBoxValidation(cmbInternalClient);
            ProcessIntegerValidation(txtInternalPort);
            ProcessIntegerValidation(txtExternalPort);
        }

        void ProcessEmptyTextValidation(TextBox txt)
        {
            if (string.IsNullOrEmpty(txt.Text))
            {
                errorProvider1.SetError(txt, "Value is Required.");
                ErrorsClean = false;
            }
        }

        void ProcessIntegerValidation(TextBox txt)
        {
            int.TryParse(txt.Text, out int x);
            if (x.ToString() != txt.Text || x < 1 || x > 65535)
            {
                errorProvider1.SetError(txt, "Value must be a valid integer between 1 and 65535.");
                ErrorsClean = false;
            }
        }

        void ProcessComboBoxValidation(ComboBox cmb)
        {
            if (string.IsNullOrEmpty(cmb.Text))
            {
                errorProvider1.SetError(cmb, "Value is Required.");
                ErrorsClean = false;
            }

            if (!cmb.Items.Contains(cmb.Text))
            {
                errorProvider1.SetError(cmb, "Value must match an entry.");
                // Note that it will only work if you're using the IP that the router sees you as!
                // (in most cases - some routers lack this security)
            }
        }

        private void PopulateLocalIPv4Addresses()
        {
            cmbInternalClient.Items.Clear();
            cmbInternalClient.Items.AddRange(NetworkHelper.AllIPv4Addresses.ToArray());
        }

        public string MappingDescription
        {
            get
            {
                return txtDescription.Text;
            }
            set
            {
                txtDescription.Text = value;
            }
        }

        public string MappingExternalIPAddress
        {
            get
            {
                return txtExternalIPAddress.Text;
            }
            set
            {
                txtExternalIPAddress.Text = value;
            }
        }

        public int MappingExternalPort
        {
            get
            {
                return int.Parse(txtExternalPort.Text);
            }
            set
            {
                txtExternalPort.Text = value.ToString();
            }
        }

        public string MappingInternalClient
        {
            get
            {
                return cmbInternalClient.Text;
            }
            set
            {
                cmbInternalClient.Text = value;
            }
        }

        public int MappingInternalPort
        {
            get
            {
                return int.Parse(txtInternalPort.Text);
            }
            set
            {
                txtInternalPort.Text = value.ToString();
            }
        }

        public bool MappingEnabled
        {
            get
            {
                return cbEnabled.Checked;
            }
            set
            {
                cbEnabled.Checked = value;
            }
        }

        public string MappingProtocol
        {
            get
            {
                return cbProtocol.Text;
            }
            set
            {
                cbProtocol.Text = value;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (ErrorsClean)
            {
                this.DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}

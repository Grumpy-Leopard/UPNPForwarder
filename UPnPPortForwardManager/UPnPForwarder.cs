﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace UPnPPortForwardManager
{
    public partial class UPnPForwarder : Form
    {
        public UPnPForwarder()
        {
            InitializeComponent();
        }

        private void UPnPForwarder_Load(object sender, EventArgs e)
        {
            RefreshPortMappings();
            lblSavedDataFileLocation.Text = SavedEntryManager.SettingsFile;
            RefreshSavedMappings();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshPortMappings();
            RefreshSavedMappings();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            UPnPMappingDialog dialog = new UPnPMappingDialog();

            dialog.MappingInternalClient = NetworkHelper.LocalIPAddress;
            dialog.MappingProtocol = "TCP";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (UPnPNATHelper.Add(dialog.MappingExternalPort, dialog.MappingProtocol, dialog.MappingInternalPort, dialog.MappingInternalClient, dialog.MappingEnabled, dialog.MappingDescription) == null)
                {
                    MessageBox.Show("The COM Module returned an error creating this mapping, please check your entries and try again.", "Mapping Not Added!");
                }

                RefreshPortMappings();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvPortMappings.SelectedItems.Count > 0)
            {
                int ExternalPortIndex = 2;
                int ProtocolIndex = 5;
                foreach (ListViewItem p in lvPortMappings.SelectedItems)
                {
                    UPnPNATHelper.Remove(int.Parse(p.SubItems[ExternalPortIndex].Text), p.SubItems[ProtocolIndex].Text);
                }
                RefreshPortMappings();
            }
        }

        void RefreshPortMappings()
        {
            lvPortMappings.Items.Clear();

            if (UPnPNATHelper.StaticPortMappings != null)
            {
                foreach (NATUPNPLib.IStaticPortMapping p in UPnPNATHelper.StaticPortMappings)
                {
                    lvPortMappings.Items.Add(new ListViewItem(new string[] { p.Description, p.ExternalIPAddress, p.ExternalPort.ToString(), p.InternalClient, p.InternalPort.ToString(), p.Protocol, p.Enabled.ToString() }));
                }
            }
        }

        void RefreshSavedMappings()
        {
            lvSavedEntries.Items.Clear();

            if (SavedEntryManager.SavedEntries != null)
            {
                foreach (SavedEntryManager.SavedEntry p in SavedEntryManager.SavedEntries)
                {
                    lvSavedEntries.Items.Add(new ListViewItem(new string[] { p.Description, p.ExternalPort.ToString(), p.InternalPort.ToString(), p.Protocol }));
                }
            }

            lvSavedEntries.Sort();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/yukienterprises/UPNPForwarder/issues");
        }

        private void btnSaveEntry_Click(object sender, EventArgs e)
        {
            if (lvPortMappings.SelectedItems.Count > 0)
            {
                int DescriptionIndex = 0;
                int ExternalPortIndex = 2;
                int InternalPortIndex = 4;
                int ProtocolIndex = 5;
                foreach (ListViewItem p in lvPortMappings.SelectedItems)
                {
                    SavedEntryManager.Add(p.SubItems[DescriptionIndex].Text, p.SubItems[ExternalPortIndex].Text, p.SubItems[InternalPortIndex].Text, p.SubItems[ProtocolIndex].Text);
                }
                RefreshSavedMappings();
            }
        }

        private void btnSavedDelete_Click(object sender, EventArgs e)
        {
            if (lvSavedEntries.SelectedItems.Count > 0)
            {
                int DescriptionIndex = 0;
                int ExternalPortIndex = 1;
                int InternalPortIndex = 2;
                int ProtocolIndex = 3;
                foreach (ListViewItem p in lvSavedEntries.SelectedItems)
                {
                    SavedEntryManager.Remove(p.SubItems[DescriptionIndex].Text, p.SubItems[ExternalPortIndex].Text, p.SubItems[InternalPortIndex].Text, p.SubItems[ProtocolIndex].Text);
                }
                RefreshSavedMappings();
            }
        }

        private void btnSavedUse_Click(object sender, EventArgs e)
        {

            if (lvSavedEntries.SelectedItems.Count > 0)
            {
                int DescriptionIndex = 0;
                int ExternalPortIndex = 1;
                int InternalPortIndex = 2;
                int ProtocolIndex = 3;


                bool errors = false;
                string Description;
                int ExternalPort;
                int InternalPort;
                string Protocol;
                foreach (ListViewItem p in lvSavedEntries.SelectedItems)
                {
                    Description = p.SubItems[DescriptionIndex].Text;
                    ExternalPort = int.Parse(p.SubItems[ExternalPortIndex].Text);
                    InternalPort = int.Parse(p.SubItems[InternalPortIndex].Text);
                    Protocol = p.SubItems[ProtocolIndex].Text;
                    if (UPnPNATHelper.Add(ExternalPort, Protocol, InternalPort, NetworkHelper.LocalIPAddress, true, Description) == null)
                    {
                        errors = true;
                    }
                }
                if (errors)
                    MessageBox.Show("The COM Module returned an error creating this mapping, please check your entries and try again.", "Mapping Not Added!");

                RefreshPortMappings();
            }
      }

        private void label2_Click(object sender, EventArgs e)
        {
            lblSavedDataFileLocation.Visible = !lblSavedDataFileLocation.Visible;
        }
    }
}

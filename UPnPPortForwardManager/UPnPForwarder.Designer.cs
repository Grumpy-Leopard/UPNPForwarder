﻿namespace UPnPPortForwardManager
{
    partial class UPnPForwarder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "DEFAULT ITEM (SHOULD NOT SEE)",
            "999.999.999.999",
            "65535",
            "999.999.999.999",
            "65535",
            "NONE",
            "FALSE"}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "DEFAULT ITEM (SHOULD NOT SEE)",
            "65535",
            "65535",
            "NONE"}, -1);
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lvPortMappings = new System.Windows.Forms.ListView();
            this.Description = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ExternalIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ExternalPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.InternalIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.InternalPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Protocol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Enabled = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblReportBug = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.lvSavedEntries = new System.Windows.Forms.ListView();
            this.SavedDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SavedExternalPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SavedInternalPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SavedProtocol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnSavedUse = new System.Windows.Forms.Button();
            this.btnSaveEntry = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSavedDelete = new System.Windows.Forms.Button();
            this.lblSavedDataFileLocation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(822, 88);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(822, 59);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Add...";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(822, 30);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 2;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lvPortMappings
            // 
            this.lvPortMappings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvPortMappings.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Description,
            this.ExternalIP,
            this.ExternalPort,
            this.InternalIP,
            this.InternalPort,
            this.Protocol,
            this.Enabled});
            this.lvPortMappings.FullRowSelect = true;
            this.lvPortMappings.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.lvPortMappings.Location = new System.Drawing.Point(12, 30);
            this.lvPortMappings.Name = "lvPortMappings";
            this.lvPortMappings.Size = new System.Drawing.Size(804, 214);
            this.lvPortMappings.TabIndex = 1;
            this.lvPortMappings.UseCompatibleStateImageBehavior = false;
            this.lvPortMappings.View = System.Windows.Forms.View.Details;
            // 
            // Description
            // 
            this.Description.Text = "Description";
            this.Description.Width = 320;
            // 
            // ExternalIP
            // 
            this.ExternalIP.Text = "External IP";
            this.ExternalIP.Width = 100;
            // 
            // ExternalPort
            // 
            this.ExternalPort.Text = "External Port";
            this.ExternalPort.Width = 75;
            // 
            // InternalIP
            // 
            this.InternalIP.Text = "Internal IP";
            this.InternalIP.Width = 100;
            // 
            // InternalPort
            // 
            this.InternalPort.Text = "Internal Port";
            this.InternalPort.Width = 75;
            // 
            // Protocol
            // 
            this.Protocol.Text = "Protocol";
            this.Protocol.Width = 55;
            // 
            // Enabled
            // 
            this.Enabled.Text = "Enabled";
            this.Enabled.Width = 55;
            // 
            // lblReportBug
            // 
            this.lblReportBug.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReportBug.AutoSize = true;
            this.lblReportBug.Location = new System.Drawing.Point(836, 458);
            this.lblReportBug.Name = "lblReportBug";
            this.lblReportBug.Size = new System.Drawing.Size(61, 13);
            this.lblReportBug.TabIndex = 9;
            this.lblReportBug.TabStop = true;
            this.lblReportBug.Text = "Report Bug";
            this.lblReportBug.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Existing Mappings";
            // 
            // lvSavedEntries
            // 
            this.lvSavedEntries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvSavedEntries.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.SavedDescription,
            this.SavedExternalPort,
            this.SavedInternalPort,
            this.SavedProtocol});
            this.lvSavedEntries.FullRowSelect = true;
            this.lvSavedEntries.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem2});
            this.lvSavedEntries.Location = new System.Drawing.Point(12, 263);
            this.lvSavedEntries.Name = "lvSavedEntries";
            this.lvSavedEntries.Size = new System.Drawing.Size(804, 208);
            this.lvSavedEntries.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvSavedEntries.TabIndex = 6;
            this.lvSavedEntries.UseCompatibleStateImageBehavior = false;
            this.lvSavedEntries.View = System.Windows.Forms.View.Details;
            // 
            // SavedDescription
            // 
            this.SavedDescription.Text = "Description";
            this.SavedDescription.Width = 575;
            // 
            // SavedExternalPort
            // 
            this.SavedExternalPort.Text = "External Port";
            this.SavedExternalPort.Width = 75;
            // 
            // SavedInternalPort
            // 
            this.SavedInternalPort.Text = "Internal Port";
            this.SavedInternalPort.Width = 75;
            // 
            // SavedProtocol
            // 
            this.SavedProtocol.Text = "Protocol";
            this.SavedProtocol.Width = 55;
            // 
            // btnSavedUse
            // 
            this.btnSavedUse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSavedUse.Location = new System.Drawing.Point(822, 263);
            this.btnSavedUse.Name = "btnSavedUse";
            this.btnSavedUse.Size = new System.Drawing.Size(75, 23);
            this.btnSavedUse.TabIndex = 7;
            this.btnSavedUse.Text = "Use";
            this.btnSavedUse.UseVisualStyleBackColor = true;
            this.btnSavedUse.Click += new System.EventHandler(this.btnSavedUse_Click);
            // 
            // btnSaveEntry
            // 
            this.btnSaveEntry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveEntry.Location = new System.Drawing.Point(822, 221);
            this.btnSaveEntry.Name = "btnSaveEntry";
            this.btnSaveEntry.Size = new System.Drawing.Size(75, 23);
            this.btnSaveEntry.TabIndex = 5;
            this.btnSaveEntry.Text = "Save";
            this.btnSaveEntry.UseVisualStyleBackColor = true;
            this.btnSaveEntry.Click += new System.EventHandler(this.btnSaveEntry_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Saved Mappings";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnSavedDelete
            // 
            this.btnSavedDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSavedDelete.Location = new System.Drawing.Point(823, 293);
            this.btnSavedDelete.Name = "btnSavedDelete";
            this.btnSavedDelete.Size = new System.Drawing.Size(75, 23);
            this.btnSavedDelete.TabIndex = 8;
            this.btnSavedDelete.Text = "Delete";
            this.btnSavedDelete.UseVisualStyleBackColor = true;
            this.btnSavedDelete.Click += new System.EventHandler(this.btnSavedDelete_Click);
            // 
            // lblSavedDataFileLocation
            // 
            this.lblSavedDataFileLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSavedDataFileLocation.AutoSize = true;
            this.lblSavedDataFileLocation.Location = new System.Drawing.Point(105, 247);
            this.lblSavedDataFileLocation.MinimumSize = new System.Drawing.Size(700, 13);
            this.lblSavedDataFileLocation.Name = "lblSavedDataFileLocation";
            this.lblSavedDataFileLocation.Size = new System.Drawing.Size(700, 13);
            this.lblSavedDataFileLocation.TabIndex = 15;
            this.lblSavedDataFileLocation.Text = "[NOT FOUND]";
            this.lblSavedDataFileLocation.Visible = false;
            // 
            // UPnPForwarder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 483);
            this.Controls.Add(this.lblSavedDataFileLocation);
            this.Controls.Add(this.btnSavedDelete);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSaveEntry);
            this.Controls.Add(this.btnSavedUse);
            this.Controls.Add(this.lvSavedEntries);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblReportBug);
            this.Controls.Add(this.lvPortMappings);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnDelete);
            this.MinimumSize = new System.Drawing.Size(825, 471);
            this.Name = "UPnPForwarder";
            this.Text = "NAT UPnP Port Forwarding Manager";
            this.Load += new System.EventHandler(this.UPnPForwarder_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ListView lvPortMappings;
        private System.Windows.Forms.ColumnHeader Description;
        private System.Windows.Forms.ColumnHeader ExternalIP;
        private System.Windows.Forms.ColumnHeader ExternalPort;
        private System.Windows.Forms.ColumnHeader InternalIP;
        private System.Windows.Forms.ColumnHeader InternalPort;
        private System.Windows.Forms.ColumnHeader Protocol;
        private System.Windows.Forms.ColumnHeader Enabled;
        private System.Windows.Forms.LinkLabel lblReportBug;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvSavedEntries;
        private System.Windows.Forms.ColumnHeader SavedDescription;
        private System.Windows.Forms.ColumnHeader SavedExternalPort;
        private System.Windows.Forms.ColumnHeader SavedInternalPort;
        private System.Windows.Forms.ColumnHeader SavedProtocol;
        private System.Windows.Forms.Button btnSavedUse;
        private System.Windows.Forms.Button btnSaveEntry;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSavedDelete;
        private System.Windows.Forms.Label lblSavedDataFileLocation;
    }
}


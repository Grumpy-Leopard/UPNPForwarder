﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace UPnPPortForwardManager
{
    public static class NetworkHelper
    {
        private static string _LocalIPAddress = string.Empty;
        private static List<string> _AllIPv4Addresses = new List<string>();
        /// <summary>
        /// This returns a String representation of the Current Machines IP Address
        /// </summary>
        public static string LocalIPAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_LocalIPAddress))
                {
                    _LocalIPAddress = AllIPv4Addresses[0];
                }
                return _LocalIPAddress;
            }
        }

        public static List<string> AllIPv4Addresses
        {
            get
            {
                if (_AllIPv4Addresses.Count==0)
                {

                    IPHostEntry host = Dns.GetHostEntry(string.Empty);
                    for (int index = 0; index < host.AddressList.Length; index++)
                    {
                        if (host.AddressList[index].AddressFamily.ToString() == ProtocolFamily.InterNetwork.ToString())
                        {
                            _AllIPv4Addresses.Add(host.AddressList[index].ToString());
                        }
                    }
                }
                return _AllIPv4Addresses;
            }
        }

    }
}
